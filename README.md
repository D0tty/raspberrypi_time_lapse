# README.md #

# What is this ? #

This script will create several directories and files (.png).
At the end you will have 1440 pictures a day which is more or less 3Go a day.
It will allow you to create time lapses.


### How do I get set up? ###

* ```git clone https://bitbucket.org/D0tty/raspberrypi_time_lapse.git```
* Configuration: Go into the new folder ```cd raspberrypi_time_lapse```
and edit the location variable as you wish ```vim timelapse.sh```
* Dependencies: you will need the latest ```raspistill``` utility and the camera module (on raspbian)
* Get it working: add a line to your crontab to execute this script every minute 

      ```* * * * * /path/to/timelapse.sh```