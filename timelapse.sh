y=$(date +%Y)
m=$(date +%m)
d=$(date +%d)
hm=$(date +%H:%M:%S)
location="/home/all/time_lapse/"
#echo "$y-$m-$d $hm"
cd $location
if [ -d "$y" ]; then
    cd "$y"
else
    mkdir "$y" && chmod 777 $y -R && cd "$y"
fi
if [ -d "$m" ]; then
    cd "$m"
else
    mkdir "$m" && chmod 777 $m -R && cd "$m"
fi
if [ -d "$d" ]; then
    cd "$d"
else
    mkdir "$d" && chmod 777 $d -R && cd "$d"
fi
raspistill -o $hm.png && chmod 777 $hm.png 
